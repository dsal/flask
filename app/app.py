import os
import pandas as pd
from slugify import slugify

# We'll render HTML templates and access data sent by POST
# using the request object from flask. Redirect and url_for
# will be used to redirect the user once the upload is done
# and send_from_directory will help us to send/show on the
# browser the file that the user just uploaded
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from werkzeug import secure_filename

# Initialize the Flask application
app = Flask(__name__)
APP__ROOT = os.path.dirname(os.path.abspath(__file__))
archivos = []

# This is the path to the upload directory
# app.config['UPLOAD_FOLDER'] = 'uploads/'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['xlsx'])
app.config['DOCS_FOLDER'] = 'docs/'


# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


# This route will show a form to perform an AJAX request
# jQuery is loaded to execute the request and update the
# value of the operation
@app.route('/')
def index():
    return render_template('index.html')


# Route that will process the file upload
@app.route('/upload', methods=['POST'])
def upload():
    target = os.path.join(APP__ROOT, 'data/')
    print(target)

    if not os.path.isdir(target):
        os.mkdir(target)

    for file in request.files.getlist("file"):
        print(file)
        filename = file.filename
        destination = "/".join([target, filename])
        print(destination)
        file.save(destination)
        return process(filename)

    return render_template("resultados.html")


# @app.route('/resultados')
# def file_list(lista=archivos):
#     return render_template('resultados.html', lista=lista)


def process(filename):
    df = pd.read_excel('data/' + filename)

    # Indicamos columna por la cual agrupar
    # groupby_muni = df.groupby('MINISTERIO RESPONSABLE')

    # Recorremos grupos
    for i, df in df.groupby('MINISTERIO RESPONSABLE'):
        # Creamos variable nombre archivo: transformamos nombre de grupo en slug
        name = df['MINISTERIO RESPONSABLE'].iloc[0]
        slug = slugify(df['MINISTERIO RESPONSABLE'].iloc[0].encode('utf-8'))
        path = os.path.join('docs/' + slug + '.xlsx')

        # Agregamos nombre de archivo y ruta a lista para generar template de resultados
        print(path)
        archivos.append((name, path))

        # print(df['MINISTERIO RESPONSABLE'].iloc[0])

        # Creamos archivo con nombre variable definida en 'val', en carpeta /docs/
        writer = pd.ExcelWriter(path, engine='xlsxwriter')
        df.to_excel(writer, 'Hoja1')
        writer.save()

    return render_template("resultados.html", archivos=archivos)


@app.route('/docs/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['DOCS_FOLDER'],
                               filename)


if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int("80"),
        debug=True
    )
